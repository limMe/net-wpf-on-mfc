本作业在Windows 8.1下使用Visual Studio 2013编写，由于使用了WPF(Windows Presentation Framework)，需要.Net 3.5以上版本支持（Windows 7 以上版本自带）。
本作由两部分组成，C++语言部分使用MFC技术，对窗口底层和数据逻辑进行处理，主要程序逻辑在HostWPFWnd文件中。
C#语言部分使用WPF技术，结合XMAL语言，绘制了更加漂亮的窗体和更快地相应用户操作。Window1.xmal.cs是主要的代码文件。

钟典 2013201384
2014年12月31日