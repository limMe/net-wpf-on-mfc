﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.ComponentModel;

namespace WPFDemo
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public class TestModel : INotifyPropertyChanged
        {
            public TestModel()
            {
            }

            private int _testValue = 0;

            public int TestValue
            {
                get { return _testValue; }
                set
                {
                    _testValue = value;
                    OnPropertyChanged("TestValue");
                }
            }

            // Declare the event
            public event PropertyChangedEventHandler PropertyChanged;

            // Create the OnPropertyChanged method to raise the event
            protected void OnPropertyChanged(string name)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(name));
                }
            }
        }

        public Window1()
        {
            InitializeComponent();
        }

        private TestModel test = new TestModel();

        public TestModel Test
        {
            get { return test; }
            set { test = value; }
        }

        public delegate void ButtonClickHandler();
        public event ButtonClickHandler ClickEvent;

        private void _btnTest_Click(object sender, RoutedEventArgs e)
        {
            //
            ClickEvent();
        }
    }

}
