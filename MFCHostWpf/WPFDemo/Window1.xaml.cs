﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.ComponentModel;

namespace WPFDemo
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        public class TestModel : INotifyPropertyChanged
        {
            public TestModel()
            {
            }

            private int _testValue = 0;

            public int TestValue
            {
                get { return _testValue; }
                set
                {
                    _testValue = value;
                    OnPropertyChanged("TestValue");
                }
            }

            // Declare the event
            public event PropertyChangedEventHandler PropertyChanged;

            // Create the OnPropertyChanged method to raise the event
            protected void OnPropertyChanged(string name)
            {
                PropertyChangedEventHandler handler = PropertyChanged;
                if (handler != null)
                {
                    handler(this, new PropertyChangedEventArgs(name));
                }
            }
        }

        public string totalStr;
        private string currentNum;
        private int dotFlag;
        private int markFlag;

        /// <summary>
        /// This will react for all clicks
        /// </summary>
        /// <param name="type">1数字 2算符 3点号 4括号 等号清除不在此列</param>
        /// <param name="value"></param>
        private void btnActions(int type, string value)
        {
            if (type == 1)
            {
                totalStr += value;
                if (markFlag == 1)
                {
                    currentNum = "";
                }
                currentNum += value;
                markFlag = 0;
                dotFlag = 0;
            }

            if(type == 2)
            {
                if(markFlag != 1)
                {
                    if(dotFlag == 1)
                    {
                        currentNum = currentNum.Substring(0, currentNum.Length - 1);
                        totalStr = totalStr.Substring(0, totalStr.Length - 1);
                        dotFlag = 0;
                    }
                    totalStr += value;
                    currentNum = "";
                    markFlag = 1;
                }
            }

            if(type == 3)
            {
                if(markFlag == 0 && dotFlag ==0)
                {
                    totalStr += ".";
                    currentNum += ".";
                    dotFlag = 1;
                }
            }

            if(type == 4)
            {
                if(dotFlag == 0)
                {
                    totalStr += value;
                }
            }

            //refresh 
            if (totalStr == "")
                totalLine.Content = "0";
            else
                totalLine.Content = totalStr;

            if (currentNum == "")
                currentLine.Content = "0";
            else
                currentLine.Content = currentNum;
        }

        public Window1()
        {
            InitializeComponent();

            totalStr = "";
            currentNum = "";
            dotFlag = 0;
            markFlag = 0;
        }

        private TestModel test = new TestModel();

        public TestModel Test
        {
            get { return test; }
            set { test = value; }
        }

        public delegate void ButtonClickHandler();
        public event ButtonClickHandler EquilEvent;

        private void _btnTest_Click(object sender, RoutedEventArgs e)
        {
            //
            
        }

        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {

        }

        private void window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            
        }

        private void Image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            btnActions(1, "7");
        }

        private void Image_MouseUp_1(object sender, MouseButtonEventArgs e)
        {
            btnActions(1, "8");
        }

        private void Image_MouseUp_2(object sender, MouseButtonEventArgs e)
        {
            btnActions(1, "9");
        }

        private void Image_MouseUp_3(object sender, MouseButtonEventArgs e)
        {
            btnActions(1, "4");
        }

        private void Image_MouseUp_4(object sender, MouseButtonEventArgs e)
        {
            btnActions(1, "5");
        }

        private void Image_MouseUp_5(object sender, MouseButtonEventArgs e)
        {
            btnActions(1, "6");
        }

        private void Image_MouseUp_6(object sender, MouseButtonEventArgs e)
        {
            btnActions(1, "1");
        }

        private void Image_MouseUp_7(object sender, MouseButtonEventArgs e)
        {
            btnActions(1, "2");
        }

        private void Image_MouseUp_8(object sender, MouseButtonEventArgs e)
        {
            btnActions(1, "3");
        }

        private void Image_MouseUp_9(object sender, MouseButtonEventArgs e)
        {
            btnActions(1, "0");
        }

        private void Image_MouseUp_10(object sender, MouseButtonEventArgs e)
        {
            btnActions(2, "/");
        }

        private void Image_MouseUp_11(object sender, MouseButtonEventArgs e)
        {
            btnActions(2, "*");
        }

        private void Image_MouseUp_12(object sender, MouseButtonEventArgs e)
        {
            btnActions(2, "-");
        }

        private void Image_MouseUp_13(object sender, MouseButtonEventArgs e)
        {
            btnActions(2, "+");
        }

        private void Image_MouseUp_14(object sender, MouseButtonEventArgs e)
        {
            btnActions(3, ".");
        }

        private void Image_MouseUp_15(object sender, MouseButtonEventArgs e)
        {
            //delete
            if (currentNum.Length > 1)
                currentNum = currentNum.Substring(0, currentNum.Length - 1);
            else
                currentNum = "";

            if (totalStr.Length > 1)
                totalStr = totalStr.Substring(0, totalStr.Length - 1);
            else
                totalStr = "";

            if (totalStr == "")
                totalLine.Content = "0";
            else
                totalLine.Content = totalStr;

            if (currentNum == "")
                currentLine.Content = "0";
            else
                currentLine.Content = currentNum;
        }

        private void Image_MouseUp_16(object sender, MouseButtonEventArgs e)
        {
            currentNum = "";
            totalStr = "";
            currentLine.Content = "0";
            totalLine.Content = "0";
        }

        public void showResult(double result)
        {
            string str = result.ToString();
            currentLine.Content = str;
            totalStr = "";//No update for displaying more naturally
        }

        public void showError()
        {
            currentLine.Content = "式子有误";
        }

        private void Image_MouseUp_17(object sender, MouseButtonEventArgs e)
        {
            EquilEvent();
        }

        private void Image_MouseUp_18(object sender, MouseButtonEventArgs e)
        {
            btnActions(4, "(");
        }

        private void Image_MouseUp_19(object sender, MouseButtonEventArgs e)
        {
            btnActions(4, ")");
        }
        
    }

}
