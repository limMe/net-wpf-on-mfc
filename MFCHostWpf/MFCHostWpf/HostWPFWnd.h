//HostWPFWnd.h
#pragma once

using namespace System;
using namespace System::Windows;
using namespace System::Windows::Interop;
using namespace System::Runtime;
using namespace WPFDemo;

public ref class CHostWPFWnd
{
public:
	CHostWPFWnd(void){};
	~CHostWPFWnd(void){};
protected:
	!CHostWPFWnd(){};

public:
	static Window1^ hostedWnd;
	static HWND hWnd;
};

HWND GetHwnd(HWND hwnd = NULL);
void Calc(); //Increase TestValue;
